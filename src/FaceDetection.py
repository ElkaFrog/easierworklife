import numpy
import cv2
import sys
import os
import time
from matplotlib import pyplot as plt
import mysql.connector



def MatchingMethod(imgVideo, photoEmploye):
    match = False
    img = cv2.imread(imgVideo, 0)
    img2 = img.copy()
    template = cv2.imread(photoEmploye, 0)
    w, h = template.shape[::-1]

    # All the 6 methods for comparison in a list ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
    #                 'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']
    methods = ['cv2.TM_SQDIFF_NORMED']

    for meth in methods:
        img = img2.copy()
        method = eval(meth)

        # Apply template Matching
        res = cv2.matchTemplate(img,template,method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

        # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
        if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
            top_left = min_loc
        else:
            top_left = max_loc
        print(min_val, max_val, min_loc, max_loc)
        bottom_right = (top_left[0] + w, top_left[1] + h)

        cv2.rectangle(img,top_left, bottom_right, 255, 2)

        #plt.subplot(121),plt.imshow(res,cmap = 'gray')
        #plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
        #plt.subplot(122),plt.imshow(img,cmap = 'gray')
        #plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
        #plt.suptitle(meth)

        #plt.show()

        return min_val

if __name__ == '__main__':

    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="",
        db='easierworklife',
        port=3306
    )

    mycursor = mydb.cursor()
    mycursor.execute("SELECT photo FROM employés")
    myresult = mycursor.fetchall()

    passPhoto = []
    idemploye = 1
    idactuel = 0

    for x in myresult:
        passPhoto.append(x[0])

    count = 0
    tabFrame = [None] * 5

    #haarcascades de base
    faceCascade = cv2.CascadeClassifier(r"C:\\opencv\\sources\\data\\haarcascades\\haarcascade_frontalface_alt.xml")

    video_capture = cv2.VideoCapture(0)

    font = cv2.FONT_HERSHEY_SIMPLEX
    topLeftCornerOfText = (10,30)
    fontScale = 1
    fontColor = (0, 150, 255)
    lineType = 2

    while True:
        # Capture frame-by-frame
        ret, frame = video_capture.read() #Ne pas péter le ret /!\
        frame2 = frame.copy()

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags=cv2.CASCADE_SCALE_IMAGE
        )

        # Draw a rectangle around the faces

        for (x, y, w, h) in faces:
            if h > 50:
                cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 150, 150), 2, 4)
                pos_rec = "Pos_X " + str(x) + " Pos_Y " + str(y)
                cv2.putText(frame, pos_rec, topLeftCornerOfText, font, fontScale, fontColor, lineType)
                print(faces)


                frameimg = "../capFace/frame%d.jpg" % count
                print(count)
                tabFrame[count] = frameimg
                count += 1

                cv2.imshow('Video', frame)
                cv2.imwrite(frameimg, frame2)

                if count == 5 :
                    count = 0
                    sumCompare = 0

                    for photoEmploye in passPhoto:

                        for fpath in tabFrame:
                            sumCompare += MatchingMethod(fpath, photoEmploye)

                            RessumCompare = sumCompare/5

                            if(RessumCompare < 0.04):
                                print("Picture Match ", RessumCompare, sumCompare, photoEmploye)
                                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2, 4)

                                myquery = "SELECT * FROM préférences where préférences.id_employe = %s" % idemploye
                                mycursor.execute(myquery)
                                myresult = mycursor.fetchall()

                                print(myresult)

                                time.sleep(1)
                            else :
                                print("Picture Not Match ", RessumCompare, sumCompare)
                    #time.sleep(0.01)

                #os.remove(frameimg)


        # Display the resulting frame
        cv2.imshow('Video', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


    # When everything is done, release the capture
    video_capture.release()
    cv2.destroyAllWindows()



